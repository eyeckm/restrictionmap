#ifndef CIRCLE_H
#define CIRCLE_H

#include <QWidget>
#include <QVector>

class Circle : public QWidget
{
        Q_OBJECT
    public:
        explicit Circle( QWidget *parent = 0 );

    protected:
        void paintEvent( QPaintEvent * event );

    private:
        const static int MARGIN = 35;
        const static int TEXT_OFFSET = 25;
        const static int LINE_LENGTH = 9;
        QVector< double > arcs;
        QVector< QString > labels;
        double circumference;

        static QPoint pointOnCircle( const QPoint & c, double r, double a );

    signals:

    public slots:
        void updateCircle( QString );

};

#endif // CIRCLE_H
