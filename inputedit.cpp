#include "inputedit.h"

InputEdit::InputEdit(QWidget *parent) :
    QLineEdit(parent)
{
}

void InputEdit::buttonPressed()
{
    emit buttonPressed( text() );
}
