#ifndef INPUTEDIT_H
#define INPUTEDIT_H

#include <QLineEdit>

class InputEdit : public QLineEdit
{
        Q_OBJECT
    public:
        explicit InputEdit(QWidget *parent = 0);

    signals:
        void buttonPressed( QString );

    public slots:
        void buttonPressed();

};

#endif // INPUTEDIT_H
