#include "circle.h"
#include <QtWidgets>
#include <algorithm>
#include <cmath>

Circle::Circle( QWidget *parent ) :
    QWidget( parent )
{
}

void Circle::paintEvent( QPaintEvent * event )
{
    QBrush background( Qt::white );
    QPen outline( Qt::black );

    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );

    painter.fillRect( event->rect(), background );
    painter.setPen( outline );


    int diameter = std::min( width(), height() ) - MARGIN * 2;
    double radius = diameter / 2.0;
    int xMargin = width() / 2 - radius;
    int yMargin = height() / 2 - radius;

    painter.drawEllipse( xMargin, yMargin, diameter, diameter );
    double startAngle = 90.0;
    QPoint c( width() / 2.0, height() / 2.0 );

    if( !arcs.empty() )
    {
        for ( int i = 0; i < arcs.size(); ++i )
        {
            double value = arcs[ i ];
            double angle = 360.0 * ( value / circumference );

            QPoint p1 = pointOnCircle( c, radius - LINE_LENGTH, startAngle );
            QPoint p2 = pointOnCircle( c, radius + LINE_LENGTH, startAngle );

            painter.setBrush( background );
            painter.drawLine( p1, p2 );
            //painter.drawPie( xMargin, yMargin, diameter, diameter, int(startAngle*16), int(angle*16));

            //QColor textColor = Qt::black;
            //painter.setPen( textColor );

            double r = radius + TEXT_OFFSET;
            //double a = startAngle + angle / 2.0; // adds label in center of arc
            double a = startAngle + angle; // adds label at end of arc
            painter.drawText( pointOnCircle( c, r, a ), labels[ i ] );

            startAngle += angle;
        }
    }
}

QPoint Circle::pointOnCircle( const QPoint &c, double r, double a )
{
    QPoint p;

    a = -a * ( 3.14 / 180.0 );
    p.setX( c.x() + r * std::cos( a ) );
    p.setY( c.y() + r * std::sin( a ) );

    return p;
}

void Circle::updateCircle( QString in )
{
    circumference = 0;
    arcs.clear();
    labels.clear();

    bool ok = true;
    QStringList values = in.split( " ", QString::SkipEmptyParts );
    for( QStringList::Iterator it = values.begin(); it != values.end(); ++it )
    {
        double a;
        QString label = it->left( 1 );
        QString remainder = it->mid( 1 );
        bool notLabeled = false;
        label.toDouble( &notLabeled );
        if( notLabeled )
        {
            label = "";
            a = it->toDouble( &ok );
        }
        else
        {
            a = remainder.toDouble( &ok );
        }

        if( !ok || a < 0 )
        {
            circumference = 0;
            arcs.clear();
            labels.clear();
            break;
        }
        else
        {
            labels.push_back( label );
            circumference += a;
            arcs.push_back( a );
        }
    }

    repaint();
}
