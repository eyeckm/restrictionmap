#-------------------------------------------------
#
# Project created by QtCreator 2014-03-12T03:20:53
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RestrictionMap
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    circle.cpp \
    inputedit.cpp

HEADERS  += mainwindow.h \
    circle.h \
    inputedit.h

FORMS    += mainwindow.ui
